<?php
  //Koneksi Database
  $server = "localhost";
  $user = "root";
  $pass = "";
  $database ="dblatihan";

  $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

  //jika tompol simpan dilakukan aksi
  if(isset($_POST['bsimpan']))//method post
  {
    //Pengujian apakah data akan diedit atau disimpan baru
    if($_GET['hal'] == "edit")
    {
        //data akan diedit
        $edit = mysqli_query($koneksi, " UPDATE jadwal  set
                                             id_jadwal ='$_POST[tidjad]',
                                             id_dosen ='$_POST[tiddos]',
                                            id_kelas ='$_POST[tidkel]',
                                            jadwal ='$_POST[tjadwal]',
                                             mata_kuliah='$_POST[tmatkul]'
                                        WHERE id_jadwal = '$_GET[id]'
                                      ");  
    if($edit)//jika edit sukses
    {
        echo "<script>
                alert('Sukses Mengedit Data!);
                document.location= 'index.php';
              </script>";
    }        
    else //jika simpan gagal
    {
        echo "<script>
                alert('Gagal Mengedit Data!);
                document.location= 'index.php';
            </script>";
        }    
    }
    else 
    {
        //data akan disimpan baru
        $simpan = mysqli_query($koneksi, "INSERT INTO jadwal  (id_jadwal, id_dosen, id_kelas, jadwal, mata_kuliah)
                                      VALUES 
                                      ('$_POST[tidjad]',
                                      '$_POST[tiddos]',
                                      '$_POST[tidkel]',
                                      '$_POST[tjadwal]',
                                      '$_POST[tmatkul]' )
                                      ");  
    if($simpan)//jika simpan sukses
    {
        echo "<script>
                alert('Sukses Menyimpan Data!);
                document.location= 'index.php';
              </script>";
    }        
    else //jika simpan gagal
    {
        echo "<script>
                alert('Gagal Menyimpan Data!);
                document.location= 'index.php';
            </script>";
        }             
    }
       
                
}

                                        
    //pengujian jika tombol edit/hapus di klik
    if(isset($_GET['hal']))//menggunakan method get
    {
        //pengujian jika edit data
        if($_GET['hal'] == "edit")//diuji apakah akan diedit atau dihapus
        {
            //tampilkan data yang akan diedit
            $tampil = mysqli_query($koneksi, "SELECT * FROM jadwal WHERE id_jadwal= '$_GET[id]'");
            $data = mysqli_fetch_array($tampil);
            if($data)
            {
                //jika data ditemukan ,maka data ditampung ke dalam variabel
                $vid_jadwal = $data['id_jadwal'];
                $vid_dosen = $data['id_dosen'];
                $vid_kelas = $data['id_kelas'];
                $vjadwal = $data['jadwal'];
                $vmata_kuliah = $data['mata_kuliah'];
               
            }
        }
        else if ($_GET['hal'] == "hapus")
        {
            //persiapan hapus data
            $hapus = mysqli_query($koneksi, "DELETE FROM jadwal  WHERE id_jadwal = '$_GET[id]'");
            if ($hapus) {
                echo "<script>
                        alert('Sukses Menghapus Data!);
                        document.location= 'index.php';
                    </script>";
            }

        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
    <a target="_blank" href="index3.php">next</a> <br>
   
<div class="container">

    <h1 class="text-center">SISTEM PENJADWALAN DOSEN</h1>
    <h2 class="text-center">PENJADWALAN DOSEN</h2>
<!-- Awal Card Form -->
    <div class="card mt-5">
  <div class="card-header bg-success ">
    Jadwal Kelas
  </div>
  <div class="card-body">
    <form method="post" action=""><!-- method post-->
        <div class="form-group">
            <label>Id Jadwal</label>
            <input type="text" name="tidjad" value="<?=@$vid_jadwal?>" class="form-control" placeholder="Input Id jadwal anda disini" required>
        </div>
        <div class="form-group">
            <label>Id Dosen</label>
            <input type="text" name="tiddos" value="<?=@$vid_dosen?>" class="form-control" placeholder="Input id anda disini" required>
        </div>
        <div class="form-group">
            <label>Id Kelas</label>
            <input type="text" name="tidkel" value="<?=@$vid_kelas?>" class="form-control" placeholder="Input id kelas anda disini" required>
        </div>
        <div class="form-group">
            <label>Jadwal</label>
            <input type="text" name="tjadwal" value="<?=@$vjadwal?>" class="form-control" placeholder="Input jadwal anda disini" required>
        </div>
        <div class="form-group">
            <label>Mata Kuliah</label>
            <input type="text" name="tmatkul" value="<?=@$vmata_kuliah?>" class="form-control" placeholder="Input mata kuliah anda disini" required>
        </div>

        <button type="submit" class="btn-success" name="bsimpan">Simpan</button>
        <button type="reset" class="btn-warning" name="breset">Batal</button>

    </form>
  </div>
</div>
<!-- Akhir Card Form -->

<!-- Awal Card Table -->
<div class="card mt-5">
  <div class="card-header bg-success ">
    Data 
  </div>
  <div class="card-body">
    
        <table class="table table-bordered table-striped">
            <tr>
                <th>No.</th> <!-- Table Header -->
                <th>Id Jadwal</th>
                <th>Id Dosen</th>
                <th>Id Kelas</th>
                <th>Jadwal</th>
                <th>Mata Kuliah</th>
                <th>Action</th>
            </tr>
            <?php
                $no = 1;
                $tampil = mysqli_query($koneksi, "SELECT * from jadwal order by id_jadwal desc"); //menampilkan data yang terakhir dimasukan dengan urutan paling atas
                while($data = mysqli_fetch_array($tampil)) :
            ?>
            <tr> <!-- Table data -->
                <td><?=$no++;?></td> <!-- Table data menambah nomor  -->
                <td><?=$data['id_jadwal']?></td><!-- menampilkan data dari database  -->
                <td><?=$data['id_dosen']?></td>
                <td><?=$data['id_kelas']?></td>
                <td><?=$data['jadwal']?></td>
                <td><?=$data['mata_kuliah']?></td> 
                <td>
                    <a href="index2.php?hal=edit&id=<?=$data['id_jadwal']?>" class= "btn btn-warning"> Edit</a>
                    <a href="index2.php?hal=hapus&id=<?=$data['id_jadwal']?>" 
                        onclick="return confirm ('Apakah Anda Yakin Ingin Menghapus?')" class="btn btn-danger"> Hapus</a>
                </td>        
            </tr>
            <?php endwhile?> <!-- penutup perulangan while -->
        </table>
  </div>
</div>
<!-- Akhir Card Table -->
</div>
<script type="text/javascript" src="js/bootstrap.min.js"><script>
</body>
</html>
