<?php
  //Koneksi Database
  $server = "localhost";
  $user = "root";
  $pass = "";
  $database ="dblatihan";

  $koneksi = mysqli_connect($server, $user, $pass, $database)or die(mysqli_error($koneksi));

  //jika tompol simpan dilakukan aksi
  if(isset($_POST['bsimpan']))//method post
  {
    //Pengujian apakah data akan diedit atau disimpan baru
    if($_GET['hal'] == "edit")
    {
        //data akan diedit
        $edit = mysqli_query($koneksi, " UPDATE dosen set
                                             id_dosen ='$_POST[tiddos]',
                                             foto_dosen ='$_POST[tfotdos]',
                                             nip_dosen ='$_POST[tnipdos]',
                                             nama_dosen ='$_POST[tnamdos]',
                                             fakultas ='$_POST[tfakdos]',
                                             prodi ='$_POST[tprodos]'
                                        WHERE id_dosen = '$_GET[id]'
                                      ");  
    if($edit)
    {
        echo "<script>
                alert('Sukses Mengedit Data!);
                document.location= 'index.php';
              </script>";
    }        
    else 
    {
        echo "<script>
                alert('Gagal Mengedit Data!);
                document.location= 'index.php';
            </script>";
        }    
    }
    else 
    {
        //data akan disimpan baru
        $simpan = mysqli_query($koneksi, "INSERT INTO dosen (id_dosen, foto_dosen, nip_dosen, nama_dosen, fakultas, prodi)
                                      VALUES 
                                      ('$_POST[tiddos]',
                                      '$_POST[tfotdos]',
                                      '$_POST[tnipdos]',
                                      '$_POST[tnamdos]',
                                      '$_POST[tfakdos]',
                                      '$_POST[tprodos]' )
                                      ");  
    if($simpan)//jika simpan sukses
    {
        echo "<script>
                alert('Sukses Menyimpan Data!);
                document.location= 'index.php';
              </script>";
    }        
    else //jika simpan gagal
    {
        echo "<script>
                alert('Gagal Menyimpan Data);
                document.location= 'index.php';
            </script>";
        }             
    }
       
                
}

                                        
    //pengujian jika tombol edit/hapus di klik
    if(isset($_GET['hal']))//menggunakan method get
    {
        //pengujian jika edit data
        if($_GET['hal'] == "edit")//diuji apakah akan diedit atau dihapus
        {
            //tampilkan data yang akan diedit
            $tampil = mysqli_query($koneksi, "SELECT * FROM dosen WHERE id_dosen = '$_GET[id]'");
            $data = mysqli_fetch_array($tampil);
            if($data)
            {
                //jika data ditemukan ,maka data ditampung ke dalam variabel
                $vid_dosen = $data['id_dosen'];
                $vfoto_dosen = $data['foto_dosen'];
                $vnip_dosen = $data['nip_dosen'];
                $vnama_dosen = $data['nama_dosen'];
                $vfakultas = $data['fakultas'];
                $vprodi = $data['prodi'];
            }
        }
        else if ($_GET['hal'] == "hapus")
        {
            //persiapan hapus data
            $hapus = mysqli_query($koneksi, "DELETE FROM dosen WHERE id_dosen = '$_GET[id]'");
            if ($hapus) {
                echo "<script>
                        alert('SUKSES MENGHAPUS DATA!);
                        document.location= 'index.php';
                    </script>";
            }

        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
    <a target="_blank" href="index2.php"> next</a>
<div class="container">

    <h1 class="text-center">SISTEM INFORMASI </h1>
    <h2 class="text-center">PENJADWALAN DOSEN</h2>


    <div class="card mt-3">
  <div class="card-header bg-success">
    Dosen
  </div>
  <div class="card-body">
    <form method="post" action="">
        <div class="form-group">
            <label>Id Dosen</label>
            <input type="text" name="tiddos" value="<?=@$vid_dosen?>" class="form-control" placeholder="Input Id anda disini" required>
        </div>
        <div class="form-group">
            <label>Foto Dosen</label>
            <input type="text" name="tfotdos" value="<?=@$vfoto_dosen?>" class="form-control" placeholder="Input Url foto anda disini" required>
        </div>
        <div class="form-group">
            <label>NIP Dosen</label>
            <input type="text" name="tnipdos" value="<?=@$vnip_dosen?>" class="form-control" placeholder="Input NIP anda disini" required>
        </div>
        <div class="form-group">
            <label>Nama Dosen</label>
            <input type="text" name="tnamdos" value="<?=@$vnama_dosen?>" class="form-control" placeholder="Input Nama anda disini" required>
        </div>
        <div class="form-group">
            <label>Fakultas</label>
            <input type="text" name="tfakdos" value="<?=@$vfakultas?>" class="form-control" placeholder="Input Fakultas anda disini" required>
        </div>
        <div class="form-group">
            <label>Program Studi</label>
            <select class="form-control" name="tprodos"> 
                <option value="<?=$vprodi?>"><?=@$vprodi?> </option>
                <option value="Manajemen Informatika">Manajemen Informatika</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Pendidikan Teknik Informatika">Pendidikan Teknik Informatika</option>
            </select>

        </div>

        <button type="submit" class="btn-success" name="bsimpan">Simpan</button>
        <button type="reset" class="btn-warning" name="breset">Batal</button>

    </form>
  </div>
</div>
<!-- Akhir Card Form -->

<!-- Awal Card Table -->
<div class="card mt-5">
  <div class="card-header bg-success ">
    Data 
  </div>
  <div class="card-body">
    
        <table class="table table-bordered table-striped">
            <tr>
                <th>No.</th> <!-- Table Header -->
                <th>Id Dosen</th>
                <th>Foto Dosen</th>
                <th>NIP Dosen</th>
                <th>Nama Dosen</th>
                <th>Fakultas</th>
                <th>Program Studi</th>
                <th>Action</th>
            </tr>
            <?php
                $no = 1;
                $tampil = mysqli_query($koneksi, "SELECT * from dosen order by id_dosen desc"); //menampilkan data yang terakhir dimasukan dengan urutan paling atas
                while($data = mysqli_fetch_array($tampil)) :
            ?>
            <tr> <!-- Table data -->
                <td><?=$no++;?></td> <!-- Table data menambah nomor  -->
                <td><?=$data['id_dosen']?></td><!-- menampilkan data dari database  -->
                <td><?=$data['foto_dosen']?></td>
                <td><?=$data['nip_dosen']?></td>
                <td><?=$data['nama_dosen']?></td>
                <td><?=$data['fakultas']?></td>
                <td><?=$data['prodi']?></td>  
                <td>
                    <a href="index.php?hal=edit&id=<?=$data['id_dosen']?>" class= "btn btn-success"> Edit</a>
                    <a href="index.php?hal=hapus&id=<?=$data['id_dosen']?>" 
                        onclick="return confirm ('Apakah Anda Yakin Ingin Menghapus?')" class="btn btn-danger"> Hapus</a>
                </td>        
            </tr>
            <?php endwhile?> <!-- penutup perulangan while -->
        </table>
  </div>
</div>
<!-- Akhir Card Table -->
</div>
<script type="text/javascript" src="js/bootstrap.min.js"><script>
</body>
</html>
